from django.urls import path
from reportada import views
from django.contrib.auth.views import LogoutView
from django.conf.urls import url
from reportada.views import *

app_name = "reportada_app"

urlpatterns = [
	path('', views.index, name="index"),
	path('sesion/', views.sesion, name='sesion'),
    path('sucursales/', views.sucursales, name='sucursales'),
    path('paquetes/', views.paquetes, name='paquetes'),
    path('pizzas/', views.pizzas, name='pizzas'),
    path('armatupizza/', views.armatupizza, name='armatupizza'),
    path('googlemaps/', views.googlemaps, name='googlemaps'),
    path('googlemaps2/', views.googlemaps2, name='googlemaps2'),
    path('googlemaps3/', views.googlemaps2, name='googlemaps3'),
	path('registrar/', views.RegistroCliente.as_view(), name='registrar'),
	path('logout/', LogoutView.as_view(), {"next_page": settings.LOGOUT_REDIRECT_URL}, name="logout"),

]